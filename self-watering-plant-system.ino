

struct HumiditySensor {
    int aPin;
     
    void setup(int sensorPin) {
      aPin = sensorPin;
    }

    int getValue() {
      return analogRead(aPin);
    }
  
    int getPct() {
      return map(getValue(), 790, 380, 0, 100);
    }
};

struct Potentiometer {
  int pin;

  void setup(int in_pin) {
    pin = in_pin;
  }

  int getValue() {
    return analogRead(pin);
  }

  int getPct() {
    return map(getValue(), 1023, 0, 0, 100);
  }
};

struct Relay {
  int pin;
  
  void setup(int relay_pin) {
    pin = relay_pin;
    pinMode(pin, OUTPUT);
  }

  void open() {
    digitalWrite(pin, HIGH);
  }

  void close() {
    digitalWrite(pin, LOW);
  }
};

struct WaterSystem {
  HumiditySensor mySensor;
  Potentiometer pot;
  Relay rel;
  bool watering;

  void setup(int sensPin, int potentioPin, int relayPin) {
    mySensor.setup(sensPin);
    pot.setup(potentioPin);
    rel.setup(relayPin);
    rel.open();
    watering = false;
  }

  void run() {
    if(mySensor.getPct() < pot.getPct()) {
      rel.close(); //closing circut, pump watering...
      watering = true;
    } else if (watering) {
      rel.open();
      watering = false;
    }
  }
};

WaterSystem wts;
int tickTime = 1000 * 30; //30 seconds

void setup() {
  Serial.begin(9600);
  wts.setup(A1, A5, 3);
}

void loop() {
  delay(tickTime);
  wts.run();
  delay(tickTime);
}
